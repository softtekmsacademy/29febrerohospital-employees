package com.sofftek.msacademy.employees.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofftek.msacademy.employees.model.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}