package com.sofftek.msacademy.employees.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofftek.msacademy.employees.model.entity.FiscalData;

public interface FiscalDataRepository extends JpaRepository<FiscalData, Integer> {

}