package com.sofftek.msacademy.employees.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "speciality")
public class Speciality {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_speciality")
	private int id_speciality;
    @Column(name = "name")
	private String name;
    
	public Speciality(int id_speciality, String name) {
		this.id_speciality = id_speciality;
		this.name = name;
	}
	public Speciality() {
	}
	
	public int getSpeciality() {
		return id_speciality;
	}
	
	public void setSpeciality(int speciality) {
		this.id_speciality = speciality;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
    
}
