package com.sofftek.msacademy.employees.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofftek.msacademy.employees.model.entity.EmployeeType;

public interface EmployeeTypeRepository extends JpaRepository<EmployeeType, Integer> {

}
