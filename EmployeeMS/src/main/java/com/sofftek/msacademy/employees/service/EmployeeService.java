package com.sofftek.msacademy.employees.service;

import java.util.List;

import com.sofftek.msacademy.employees.model.entity.Employee;
import com.sofftek.msacademy.employees.service.impl.ResourceNotFoundException;

public interface EmployeeService {
    Employee createEmployee(Employee employee);

    Employee getEmployeeById(int employeeId) throws ResourceNotFoundException;
    
    Boolean deleteEmployee(int id) throws ResourceNotFoundException;
    
    Employee updateEmployee(Employee employee);
    
    List<Employee> getAllEmployees();
}
