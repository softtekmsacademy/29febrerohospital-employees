package com.sofftek.msacademy.employees.model.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payroll")
public class Payroll {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_payroll")
	private int id_payroll;
	@ManyToOne
	@JoinColumn(name = "employee")
	private Employee employee;
	@Column(name = "hour_work")
	private float hoursWorked;
	@Column(name = "salary")
	private double salary;
	@Column(name = "hourly_wage")
	private double hourlyWage;
	@Column(name = "extra_hours_worked")
	private double extraHours;
	@Column(name = "taxes_percentage")
	private double taxesPercentage;
	@Column(name = "date")
	private Date date;
    
	public Payroll(int id_payroll, Employee employee, float hoursWorked, double salary, double hourlyWage,
			double extraHours, double taxesPercentage, Date date) {
		this.id_payroll = id_payroll;
		this.employee = employee;
		this.hoursWorked = hoursWorked;
		this.salary = salary;
		this.hourlyWage = hourlyWage;
		this.extraHours = extraHours;
		this.taxesPercentage = taxesPercentage;
		this.date = date;
	}

	public Payroll() {
	}

	public int getId_payroll() {
		return id_payroll;
	}

	public void setId_payroll(int id_payroll) {
		this.id_payroll = id_payroll;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public float getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(float hoursWorked) {
		this.hoursWorked = hoursWorked;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getHourlyWage() {
		return hourlyWage;
	}

	public void setHourlyWage(double hourlyWage) {
		this.hourlyWage = hourlyWage;
	}

	public double getExtraHours() {
		return extraHours;
	}

	public void setExtraHours(double extraHours) {
		this.extraHours = extraHours;
	}

	public double getTaxesPercentage() {
		return taxesPercentage;
	}

	public void setTaxesPercentage(double taxesPercentage) {
		this.taxesPercentage = taxesPercentage;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
    
	
}
