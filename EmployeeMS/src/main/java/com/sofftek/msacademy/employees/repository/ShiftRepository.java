package com.sofftek.msacademy.employees.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofftek.msacademy.employees.model.entity.Shift;

public interface ShiftRepository extends JpaRepository<Shift, Integer> {

}