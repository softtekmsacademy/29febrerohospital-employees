package com.sofftek.msacademy.employees.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.sofftek.msacademy.employees.model.entity.Employee;
import com.sofftek.msacademy.employees.service.EmployeeService;
import com.sofftek.msacademy.employees.repository.EmployeeRepository;
@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty(name = "coreSize", value = "10"),
                @HystrixProperty(name = "maxQueueSize", value = "-1")
        },
        commandProperties = {
        		@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "9000"),
                @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
                @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
                @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
                @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
        }
)
public class EmployeeServiceImpl implements EmployeeService {
	
	    @Autowired
	    private EmployeeRepository employeeRepository;
	    
	    @HystrixCommand(threadPoolKey = "createEmpThreadPool")
	    @Override
	    public Employee createEmployee(Employee employee) {
	        return employeeRepository.save(employee);
	    }
	    
	    @HystrixCommand(threadPoolKey = "updateEmpThreadPool")
	    @Override
	    public Employee updateEmployee(Employee employee) {
	        Optional<Employee> employeeDb = employeeRepository.findById(employee.getId_employee());
	        if (employeeDb.isPresent()) {
	            Employee employeeUpdate = employeeDb.get();
	            employeeUpdate.setBirth_date(employee.getBirth_date());
	            employeeUpdate.setFirst_name(employee.getFirst_name());
	            employeeUpdate.setFiscal_data(employee.getFiscal_data());
	            employeeUpdate.setRole(employee.getRole());
	            employeeUpdate.setType(employee.getType());
	            employeeUpdate.setSecond_last_name(employee.getSecond_last_name());
	            employeeUpdate.setLast_name(employee.getLast_name());
	            employeeUpdate.setSpeciality(employee.getSpeciality());
	            employeeRepository.save(employeeUpdate);
	            return employeeUpdate;
	        } else {
	            throw new ResourceNotFoundException("Record not found with id : " + employee.getId_employee());
	        }
	    }
	    @HystrixCommand(threadPoolKey = "getAllEmpThreadPool" , fallbackMethod = "employeeFallbackMethod")
	    @Override
	    public List<Employee> getAllEmployees() {
	        return this.employeeRepository.findAll();
	    }
	    
		@HystrixCommand(threadPoolKey = "getEmpThreadPool" , fallbackMethod = "employeeFallbackMethod")
	    @Override
	    public Employee getEmployeeById(int employeeId) {
	        Optional<Employee> productDb = employeeRepository.findById(employeeId);
	        if (productDb.isPresent()) {
	            return productDb.get();
	        } else {
	           throw new ResourceNotFoundException("Record not found with id : " + employeeId);
	        }
	    }
		
		@HystrixCommand(threadPoolKey = "getEmpThreadPool")
	    @Override
	    public Boolean deleteEmployee(int employeeId) throws ResourceNotFoundException {
	        Optional<Employee> productDb = employeeRepository.findById(employeeId);
	        if (productDb.isPresent()) {
	            this.employeeRepository.delete(productDb.get());
	            return true;
	        } else {
		        return false;
	        }
	    }
		
		public Employee employeeFallbackMethod(int id) {
			Employee defaultEmpl=new Employee();
			defaultEmpl.setFirst_name("Database Error");
			defaultEmpl.setId_employee(id);
			return defaultEmpl;
		}
		
		public List<Employee> employeeFallbackMethod() {
			Employee defaultEmpl= new Employee();
			defaultEmpl.setFirst_name("Database Error");
			defaultEmpl.setId_employee(1);
			List<Employee> listEmployees= new ArrayList<Employee>();
			listEmployees.add(defaultEmpl);
			return listEmployees;
		}
		
}
