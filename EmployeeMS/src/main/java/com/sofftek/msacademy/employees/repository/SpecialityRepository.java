package com.sofftek.msacademy.employees.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofftek.msacademy.employees.model.entity.Speciality;

public interface SpecialityRepository extends JpaRepository<Speciality, Integer> {

}