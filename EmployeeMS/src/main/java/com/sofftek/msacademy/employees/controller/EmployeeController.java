package com.sofftek.msacademy.employees.controller;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.sofftek.msacademy.employees.model.entity.Employee;
import com.sofftek.msacademy.employees.model.entity.Payroll;
import com.sofftek.msacademy.employees.model.entity.Role;
import com.sofftek.msacademy.employees.service.EmployeeService;
import com.sofftek.msacademy.employees.service.PayrollService;
import com.sofftek.msacademy.employees.service.RoleService;
import com.sofftek.msacademy.employees.service.impl.ResourceNotFoundException;

@RestController
public class EmployeeController {
	 @Autowired
	 private EmployeeService employeeService;
	 @Autowired
	 private PayrollService payrollService;
	 
	  @PostMapping("/employees")
	  public ResponseEntity postEmployees( @Valid @RequestBody Employee employee)
	      throws ResourceAccessException {
		  return ResponseEntity.ok().body(employeeService.createEmployee(employee));
	  }
	  
	  @GetMapping("/employees")
	  public ResponseEntity<List<Employee>> getEmployees() {
		  return ResponseEntity.ok().body(employeeService.getAllEmployees());
	  }
	  
	  @PutMapping("/employees")
	  public ResponseEntity putEmployees( @Valid @RequestBody Employee employee)
	      throws ResourceAccessException {		  
		  return ResponseEntity.ok().body(employeeService.updateEmployee(employee));
	  }
	  
	  @DeleteMapping("/employees/{employeeId}")
	  public ResponseEntity deleteEmployees(@PathVariable(value = "employeeId") Integer employeeId)
	      throws ResourceAccessException {
		  employeeService.deleteEmployee(employeeId);
		  return ResponseEntity.ok().body(null);
	  }
	  
	  @GetMapping("/employees/{employeeId}")
	  public ResponseEntity getUserById(@PathVariable(value = "employeeId") Integer employeeId) 
	  {	  
		try {
			if(employeeService.getEmployeeById(employeeId) !=  null) {
				  return ResponseEntity.ok().body(employeeService.getEmployeeById(employeeId));
			  }
		} catch (ResourceNotFoundException e) {
			// TODO Auto-generated catch block
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The resource doesnt exist");		
		}
		  return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The resource doesnt exist");
	  }
	  	  
	  @GetMapping("/employees/{employeeId}/payroll")
	  public ResponseEntity getEmployeePayroll(@PathVariable(value = "employeeId") Integer employeeId )
	  {	  
		  return ResponseEntity.ok().body(payrollService.getPayrollByIdEmployee((int)employeeId));
	  }
	   
	  @PostMapping("/employees/payroll")
	  public ResponseEntity postRole( @Valid @RequestBody Payroll payroll)
	      throws ResourceAccessException {
		  return ResponseEntity.ok().body(payrollService.createPayroll(payroll));
	  }
	  
	  @PutMapping("/employees/payroll")
	  public ResponseEntity updatePayroll( @Valid @RequestBody Payroll payroll)
	      throws ResourceAccessException {
		  return ResponseEntity.ok().body(payrollService.updatePayroll(payroll));
	  }
	  
	  @DeleteMapping("/employees/payroll/{payrollId}")
	  public ResponseEntity deletePayroll( @PathVariable(value = "payrollId") Integer payrollId)
	      throws ResourceAccessException {
		  return ResponseEntity.ok().body(payrollService.deletePayroll(payrollId));
	  }
	  
	  @PostMapping("/employees/shift")
	  public ResponseEntity postShift( @Valid @RequestBody Payroll payroll)
		      throws ResourceAccessException {
			  return ResponseEntity.ok().body(payrollService.createPayroll(payroll));
	  }
	  	  	  
	   
}