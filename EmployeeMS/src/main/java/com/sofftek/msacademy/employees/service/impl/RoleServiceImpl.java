package com.sofftek.msacademy.employees.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofftek.msacademy.employees.model.entity.Role;
import com.sofftek.msacademy.employees.service.RoleService;
import com.sofftek.msacademy.employees.repository.RoleRepository;
@Service
@Transactional
public class RoleServiceImpl implements RoleService {
	    @Autowired
	    private RoleRepository roleRepository;
	    @Override
	    public Role createRole(Role role) {
	        return roleRepository.save(role);
	    }

	    @Override
	    public Role updateRole(Role role) {
	        Optional<Role> roleDb = roleRepository.findById(role.getId_role());
	        if (roleDb.isPresent()) {
	            Role roleUpdate = roleDb.get();
	            roleUpdate.setId_role(role.getId_role());;
	            roleUpdate.setName(role.getName());
	            roleRepository.save(roleUpdate);
	            return roleUpdate;
	        } else {
	            throw new ResourceNotFoundException("Record not found with id : " + role.getId_role());
	        }
	    }

	    @Override
	    public List <Role> getAllRoles() {
	        return this.roleRepository.findAll();
	    }

	    @Override
	    public Role getRoleById(int roleId) throws ResourceNotFoundException {
	        Optional <Role> roleDB = roleRepository.findById(roleId);

	        if (roleDB.isPresent()) {
	            return roleDB.get();
	        } else {
	            throw new ResourceNotFoundException("Record not found with id : " + roleId);
	        }
	    }

	    @Override
	    public Boolean deleteRole(int roleId) throws ResourceNotFoundException {
	        Optional <Role> roleDb = roleRepository.findById(roleId);
	        if (roleDb.isPresent()) {
	            this.roleRepository.delete(roleDb.get());
	            return true;
	        } else {
		        return false;
	        }
	    }

		@Override
		public boolean existRole(int id) {
			// TODO Auto-generated method stub
			return false;
		}
}