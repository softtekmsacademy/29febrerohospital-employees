package com.sofftek.msacademy.employees.service;

import java.util.List;

import com.sofftek.msacademy.employees.model.entity.Payroll;
import com.sofftek.msacademy.employees.service.impl.ResourceNotFoundException;



public interface PayrollService {
    Payroll createPayroll(Payroll employee);

    Payroll getPayrollById(int payrollId) throws ResourceNotFoundException;
    
    Boolean deletePayroll(int id) throws ResourceNotFoundException;
    
    Payroll updatePayroll(Payroll payroll);
    
    List<Payroll> getAllPayrolls();
    
    List<Payroll> getPayrollByIdEmployee(int employeeId) throws ResourceNotFoundException;
}
