package com.sofftek.msacademy.employees.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee_has_shifts")
public class EmployeeShift {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_employees_has_shifts")
	private int id_employeeShift;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "id_employees")
	private Employee employee;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "id_shifts")
	private Shift shift;
    @Column(name = "start_day_of_week")
	private int start_day_of_week;
    @Column(name = "end_day_of_week")
	private int end_day_of_week;
    @Column(name = "active")
	private int active;
	
	public EmployeeShift(int id_employeeShift, Employee employee, Shift shift, int start_day_of_week, int end_day_of_week,
			int active) {
		this.id_employeeShift = id_employeeShift;
		this.employee = employee;
		this.shift = shift;
		this.start_day_of_week = start_day_of_week;
		this.end_day_of_week = end_day_of_week;
		this.active = active;
	}
	
	public EmployeeShift() {
	}
	
	public int getId_employee() {
		return id_employeeShift;
	}
	
	public void setId_employee(int id_employeeShift) {
		this.id_employeeShift = id_employeeShift;
	}
	
	public Employee getEmployee() {
		return employee;
	}
	
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public Shift getShift() {
		return shift;
	}
	
	public void setShift(Shift shift) {
		this.shift = shift;
	}
	
	public int getStart_day_of_week() {
		return start_day_of_week;
	}
	
	public void setStart_day_of_week(int start_day_of_week) {
		this.start_day_of_week = start_day_of_week;
	}
	
	public int getEnd_day_of_week() {
		return end_day_of_week;
	}
	
	public void setEnd_day_of_week(int end_day_of_week) {
		this.end_day_of_week = end_day_of_week;
	}
	
	public int getActive() {
		return active;
	}
	
	public void setActive(int active) {
		this.active = active;
	}
	
}
