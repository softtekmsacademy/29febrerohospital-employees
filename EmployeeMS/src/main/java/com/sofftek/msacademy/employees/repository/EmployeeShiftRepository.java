package com.sofftek.msacademy.employees.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofftek.msacademy.employees.model.entity.EmployeeShift;

public interface EmployeeShiftRepository extends JpaRepository<EmployeeShift, Integer> {

}
