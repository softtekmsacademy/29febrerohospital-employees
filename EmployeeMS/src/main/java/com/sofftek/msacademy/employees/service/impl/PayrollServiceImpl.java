package com.sofftek.msacademy.employees.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sofftek.msacademy.employees.model.entity.Payroll;
import com.sofftek.msacademy.employees.repository.PayrollRepository;
import com.sofftek.msacademy.employees.service.PayrollService;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty(name = "coreSize", value = "10"),
                @HystrixProperty(name = "maxQueueSize", value = "-1")
        },
        commandProperties = {
        		@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "9000"),
                @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
                @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
                @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
                @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
        }
)
public class PayrollServiceImpl implements PayrollService {
    @Autowired
    private PayrollRepository payrollRepository;
    
    @HystrixCommand(threadPoolKey = "createPayrollThreadPool")
    @Override
    public Payroll createPayroll(Payroll payroll) {
        return payrollRepository.save(payroll);
    }
    @HystrixCommand(threadPoolKey = "updatePayrollThreadPool")
    @Override
    public Payroll updatePayroll(Payroll payroll) {
        Optional<Payroll> payrollDb = payrollRepository.findById(payroll.getId_payroll());
        if (payrollDb.isPresent()) {
        	Payroll payrollUpdate = payrollDb.get();
        	payrollUpdate.setEmployee(payroll.getEmployee());
        	payrollUpdate.setDate(payroll.getDate());
        	payrollUpdate.setExtraHours(payroll.getExtraHours());
        	payrollUpdate.setHoursWorked(payroll.getHoursWorked());
        	payrollUpdate.setSalary(payroll.getSalary());
        	payrollUpdate.setTaxesPercentage(payroll.getTaxesPercentage());
            payrollRepository.save(payrollUpdate);
            return payrollUpdate;
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + payroll.getId_payroll());
        }
    }  
    @HystrixCommand( fallbackMethod = "payrollFallbackMethod" , threadPoolKey = "getAllPayrollThreadPool")
    @Override
    public List<Payroll> getAllPayrolls() {
        return this.payrollRepository.findAll();
    }
    @HystrixCommand(threadPoolKey = "getPayrollByIdThreadPool")
    @Override
    public Payroll getPayrollById(int payrollId) throws ResourceNotFoundException {
        Optional<Payroll> payrollDb = payrollRepository.findById(payrollId);

        if (payrollDb.isPresent()) {
            return payrollDb.get();
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + payrollId);
        }
    }
    
    @HystrixCommand( fallbackMethod = "payrollFallbackMethod", threadPoolKey = "getPayrollThreadPool")
    @Override
    public List <Payroll> getPayrollByIdEmployee(int employeeId) throws ResourceNotFoundException {
        List <Payroll> payrollDb = payrollRepository.findByIdEmployee(employeeId);
        if (payrollDb!=null) {
            return payrollDb;
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + employeeId);
        }
    }
    
    @HystrixCommand(threadPoolKey = "deletePayrollThreadPool")
    @Override
    public Boolean deletePayroll(int payrollId) throws ResourceNotFoundException {
        Optional <Payroll> payrollDb = this.payrollRepository.findById(payrollId);
        if (payrollDb.isPresent()) {
            this.payrollRepository.delete(payrollDb.get());
            return true;
        } else {
	        return false;
        }
    }

	public List<Payroll> payrollFallbackMethod(int employeeId) {
		Payroll defaultPayroll= new Payroll();
		defaultPayroll.setId_payroll(0);
		defaultPayroll.setHoursWorked(-20);
		defaultPayroll.setSalary(-100);
		List<Payroll> listPayroll= new ArrayList<Payroll>();
		listPayroll.add(defaultPayroll);
		return listPayroll;
	}
	
	public List<Payroll> payrollFallbackMethod() {
		Payroll defaultPayroll= new Payroll();
		defaultPayroll.setId_payroll(0);
		defaultPayroll.setHoursWorked(-20);
		defaultPayroll.setSalary(-100);
		List<Payroll> listPayroll= new ArrayList<Payroll>();
		listPayroll.add(defaultPayroll);
		return listPayroll;
	}
		
}
