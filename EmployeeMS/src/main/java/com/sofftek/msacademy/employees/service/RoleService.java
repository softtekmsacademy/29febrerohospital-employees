package com.sofftek.msacademy.employees.service;

import java.util.List;

import com.sofftek.msacademy.employees.model.entity.Employee;
import com.sofftek.msacademy.employees.model.entity.Role;
import com.sofftek.msacademy.employees.service.impl.ResourceNotFoundException;

public interface RoleService {
    Role createRole(Role role);

    Role getRoleById(int roleId) throws ResourceNotFoundException;
    
    boolean existRole(int id);
    
    Boolean deleteRole(int id) throws ResourceNotFoundException;
    
    Role updateRole(Role role);
    
    List<Role> getAllRoles();

}
