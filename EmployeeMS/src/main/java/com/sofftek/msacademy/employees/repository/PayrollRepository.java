package com.sofftek.msacademy.employees.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sofftek.msacademy.employees.model.entity.Payroll;

public interface PayrollRepository extends JpaRepository<Payroll, Integer> {

	@Query(value="SELECT * FROM Payroll WHERE employee = :employeeId", nativeQuery=true)
	List<Payroll> findByIdEmployee(@Param("employeeId")  Integer employeeId);

}
