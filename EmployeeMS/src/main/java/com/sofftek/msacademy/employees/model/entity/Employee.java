package com.sofftek.msacademy.employees.model.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_employee")
	private Integer id_employee;
    @Column(name = "first_name")
	private String first_name;
    @Column(name = "last_name")
	private String last_name;
    @Column(name = "second_last_name")
	private String second_last_name;
    @ManyToOne
    @JoinColumn(name = "role")
	private Role role;
    @ManyToOne
    @JoinColumn(name = "type")
	private EmployeeType type;
    @ManyToOne
    @JoinColumn(name = "fiscal_data")
	private FiscalData fiscal_data;
    @Column(name = "birth_date")
	private Date birth_date;
    @ManyToOne
    @JoinColumn(name = "speciality")
	private Speciality speciality;
	
	public Employee(int id_employee, String first_name,String last_name, String second_last_name, Role role, EmployeeType type,
			FiscalData fiscal_data, Date birth_date, Speciality speciality) {
		this.id_employee = id_employee;
		this.first_name = first_name;
		this.last_name=last_name;
		this.second_last_name = second_last_name;
		this.role = role;
		this.type = type;
		this.fiscal_data = fiscal_data;
		this.birth_date = birth_date;
		this.speciality = speciality;
	}
	
		
	public Employee() {
	}


	public int getId_employee() {
		return id_employee;
	}
	
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}
	
	public String getFirst_name() {
		return first_name;
	}
	
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	
	
	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public String getSecond_last_name() {
		return second_last_name;
	}
	
	public void setSecond_last_name(String second_last_name) {
		this.second_last_name = second_last_name;
	}
	
	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
	public EmployeeType getType() {
		return type;
	}
	
	public void setType(EmployeeType type) {
		this.type = type;
	}
	
	public FiscalData getFiscal_data() {
		return fiscal_data;
	}
	
	public void setFiscal_data(FiscalData fiscal_data) {
		this.fiscal_data = fiscal_data;
	}
	
	public Date getBirth_date() {
		return birth_date;
	}
	
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	
	public Speciality getSpeciality() {
		return speciality;
	}
	
	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}
	
}
