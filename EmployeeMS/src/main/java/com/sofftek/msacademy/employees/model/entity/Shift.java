package com.sofftek.msacademy.employees.model.entity;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "speciality")
public class Shift {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_shift")
	private int id_shift;
    @Column(name = "name")
	private String name;
    @Column(name = "start_time")
	private Time start_time;
    @Column(name = "end_time")
	private Time end_time;
	
	public Shift(int id_shift, String name, Time start_time, Time end_time) {
		this.id_shift = id_shift;
		this.name = name;
		this.start_time = start_time;
		this.end_time = end_time;
	}
	public Shift() {
	}
	public int getId_shift() {
		return id_shift;
	}
	public void setId_shift(int id_shift) {
		this.id_shift = id_shift;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Time getStart_time() {
		return start_time;
	}
	public void setStart_time(Time start_time) {
		this.start_time = start_time;
	}
	public Time getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Time end_time) {
		this.end_time = end_time;
	}
	
	
}
